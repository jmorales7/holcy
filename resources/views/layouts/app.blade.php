<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- The above 4 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <!-- Title  -->
    <title>Karl - Fashion Ecommerce Template | Home</title>

    <!-- Favicon  -->
    <link rel="icon" href="{{asset('img/core-img/favicon.ico')}}">

    <!-- Core Style CSS -->
    <link rel="stylesheet" href="{{asset('css/core-style.css')}}">
    {{--<link rel="stylesheet" href="style.css">--}}

    <!-- Responsive CSS -->
    <link href="{{'css/responsive.css'}}" rel="stylesheet">

</head>

<body>
@include('layouts.includes.sidebar')

<div id="wrapper">

    <!-- ****** Header Area Start ****** -->
    @include('layouts.includes.header')
    <!-- ****** Header Area End ****** -->

    <!-- ****** Top Discount Area Start ****** -->
    @include('layouts.includes.notices')
    <!-- ****** Top Discount Area End ****** -->

    @yield('section')

    <!-- ****** Footer Area Start ****** -->
    @include('layouts.includes.footer')
    <!-- ****** Footer Area End ****** -->
</div>
<!-- /.wrapper end -->

<!-- jQuery (Necessary for All JavaScript Plugins) -->
<script src="{{asset('js/jquery/jquery-2.2.4.min.js')}}"></script>
<!-- Popper js -->
<script src="{{asset('js/popper.min.js')}}"></script>
<!-- Bootstrap js -->
<script src="{{asset('js/bootstrap.min.js')}}"></script>
<!-- Plugins js -->
<script src="{{asset('js/plugins.js')}}"></script>
<!-- Active js -->
<script src="{{asset('js/active.js')}}"></script>

</body>

</html>
